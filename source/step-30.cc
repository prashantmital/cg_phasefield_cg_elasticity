/*
This code is licensed under the "GNU GPL version 2 or later". See
license.txt or https://www.gnu.org/licenses/gpl-2.0.html

Copyright 2012-2014: Thomas Wick
*/

// This program (is based on deal.II step-30)
// Phase-field crack propagation in elasticity
// The specific example is the single edge notched tension
// test (see Miehe et al. 2010a,b)

// A TODO list can be found at the end

#include <base/quadrature_lib.h>
#include <base/function.h>
#include <base/utilities.h>
            
#include <lac/vector.h>
#include <lac/sparse_matrix.h>
#include <lac/constraint_matrix.h>
#include <lac/block_sparse_matrix.h>
#include <lac/sparse_direct.h>

#include <grid/tria.h>
#include <grid/grid_generator.h>
#include <grid/grid_out.h>
#include <grid/grid_refinement.h>
#include <grid/tria_accessor.h>
#include <grid/tria_iterator.h>
#include <grid/tria_boundary_lib.h>
#include <grid/grid_in.h>
#include <grid/grid_tools.h>

#include <fe/fe_values.h>
#include <fe/fe_system.h>
#include <fe/mapping_q1.h>
#include <fe/fe_dgq.h>
#include <fe/fe_dgp.h>
#include <fe/fe_q.h>

#include <dofs/dof_handler.h>
#include <dofs/dof_accessor.h>
#include <dofs/dof_tools.h>
#include <dofs/dof_renumbering.h>

#include <numerics/data_out.h>
#include <numerics/solution_transfer.h>
#include <numerics/vector_tools.h>
#include <numerics/matrix_tools.h>

#include <base/timer.h>

#include <iostream>
#include <fstream>
#include <algorithm>

#include "input_handler.h"

using namespace dealii;

//-----------------------------------------------------------------------------
// Enumeration to specify which case to run
enum class TestCase {
    none,
    miehe_tension,
    miehe_shear,
    symmetric_bending
};

//-----------------------------------------------------------------------------
// Enumeration to specify type of interpolation for pf_extra
enum class LinearizationScheme {
    none,
    simple,
    interpolation
};

//-----------------------------------------------------------------------------
// Enumeration to specify type of outer-loop optimization
enum class OptimizationType {
    none,
    simple,
    auglag
};

//------------------------------------------------------------------------------
// Interpolation function for PF
inline double interpolate_pf(const double old_timestep_pf,
        const double old_old_timestep_pf,
        const double old_timestep,
        const double old_old_timestep)
{
    return old_old_timestep_pf +
        (old_timestep + old_old_timestep) / (old_old_timestep) * (old_timestep_pf - old_old_timestep_pf);
}

//------------------------------------------------------------------------------
// Normalization of a PF variable between 0 and 1
inline double normalize_pf(const double pf)
{
    if (pf > 1.0)
        return 1.0;
    else if (pf < 0.0)
        return 0.0;
    else
        return pf;
}

// Define some tensors for cleaner notation later.
namespace Tensors
{

  template <int dim>
  inline Tensor<1, dim>
  get_grad_pf (
    unsigned int q,
    const std::vector<std::vector<Tensor<1, dim> > > &old_solution_grads)
  {
    Tensor<1, dim> grad_pf;
    grad_pf[0] = old_solution_grads[q][dim][0];
    grad_pf[1] = old_solution_grads[q][dim][1];

    return grad_pf;
  }

  template <int dim>
  inline Tensor<2, dim>
  get_grad_u (
    unsigned int q,
    const std::vector<std::vector<Tensor<1, dim> > > &old_solution_grads)
  {
    Tensor<2, dim> structure_continuation;
    structure_continuation[0][0] = old_solution_grads[q][0][0];
    structure_continuation[0][1] = old_solution_grads[q][0][1];
    structure_continuation[1][0] = old_solution_grads[q][1][0];
    structure_continuation[1][1] = old_solution_grads[q][1][1];

    return structure_continuation;
  }

  template <int dim>
  inline Tensor<2, dim>
  get_Identity ()
  {
    Tensor<2, dim> identity;
    identity[0][0] = 1.0;
    identity[0][1] = 0.0;
    identity[1][0] = 0.0;
    identity[1][1] = 1.0;

    return identity;
  }

  template <int dim>
  inline Tensor<1, dim>
  get_u (
    unsigned int q,
    const std::vector<Vector<double> > &old_solution_values)
  {
    Tensor<1, dim> u;
    u[0] = old_solution_values[q](0);
    u[1] = old_solution_values[q](1);

    return u;
  }


  template <int dim>
  inline double
  get_pf (
    unsigned int q,
    const std::vector<Vector<double> > &old_solution_values)
  {
    double pf = old_solution_values[q][dim];
    return pf;
  }

  template <int dim>
  inline double
  get_lambda_penal (
    unsigned int q,
    const std::vector<Vector<double> > &old_lambda_penal_func_values)
  {
    double lpf = old_lambda_penal_func_values[q][dim];
    return lpf;
  }

}

//-----------------------------------------------------------------------------
// Functions for spectral decomposition of stresses
template <int dim>
void eigen_vectors_and_values(
  double &E_eigenvalue_1, double &E_eigenvalue_2,
  Tensor<2,dim> &ev_matrix,
  const Tensor<2,dim> &matrix)
{

  // Compute eigenvectors
  Tensor<1,dim> E_eigenvector_1;
  Tensor<1,dim> E_eigenvector_2;
  if (std::abs(matrix[0][1]) < 1e-10*std::abs(matrix[0][0])
      || std::abs(matrix[0][1]) < 1e-10*std::abs(matrix[1][1]))
    {
      // E is close to diagonal
      E_eigenvalue_1 = matrix[0][0];
      E_eigenvector_1[0]=1;
      E_eigenvector_1[1]=0;
      E_eigenvalue_2 = matrix[1][1];
      E_eigenvector_2[0]=0;
      E_eigenvector_2[1]=1;
    }
  else
    {
      double sq = std::sqrt((matrix[0][0] - matrix[1][1]) * (matrix[0][0] - matrix[1][1]) + 4.0*matrix[0][1]*matrix[1][0]);
      E_eigenvalue_1 = 0.5 * ((matrix[0][0] + matrix[1][1]) + sq);
      E_eigenvalue_2 = 0.5 * ((matrix[0][0] + matrix[1][1]) - sq);

      E_eigenvector_1[0] = 1.0/(std::sqrt(1 + (E_eigenvalue_1 - matrix[0][0])/matrix[0][1] * (E_eigenvalue_1 - matrix[0][0])/matrix[0][1]));
      E_eigenvector_1[1] = (E_eigenvalue_1 - matrix[0][0])/(matrix[0][1] * (std::sqrt(1 + (E_eigenvalue_1 - matrix[0][0])/matrix[0][1] * (E_eigenvalue_1 - matrix[0][0])/matrix[0][1])));
      E_eigenvector_2[0] = 1.0/(std::sqrt(1 + (E_eigenvalue_2 - matrix[0][0])/matrix[0][1] * (E_eigenvalue_2 - matrix[0][0])/matrix[0][1]));
      E_eigenvector_2[1] = (E_eigenvalue_2 - matrix[0][0])/(matrix[0][1] * (std::sqrt(1 + (E_eigenvalue_2 - matrix[0][0])/matrix[0][1] * (E_eigenvalue_2 - matrix[0][0])/matrix[0][1])));
    }

  ev_matrix[0][0] = E_eigenvector_1[0];
  ev_matrix[0][1] = E_eigenvector_2[0];
  ev_matrix[1][0] = E_eigenvector_1[1];
  ev_matrix[1][1] = E_eigenvector_2[1];

  // Sanity check if orthogonal
  double scalar_prod = 1.0e+10;
  scalar_prod = E_eigenvector_1[0] * E_eigenvector_2[0] + E_eigenvector_1[1] * E_eigenvector_2[1];

  if (scalar_prod > 1.0e-6)
    {
      std::cout << "Seems not to be orthogonal" << std::endl;
      abort();
    }
}


template <int dim>
void decompose_stress(
  Tensor<2,dim> &stress_term_plus,
  Tensor<2,dim> &stress_term_minus,
  const Tensor<2, dim> &E,
  const double tr_E,
  const Tensor<2, dim> &E_LinU,
  const double tr_E_LinU,
  const double lame_coefficient_lambda,
  const double lame_coefficient_mu,
  const bool derivative)
{
  static const Tensor<2, dim> Identity =
    Tensors::get_Identity<dim>();

  Tensor<2, dim> zero_matrix;
  zero_matrix.clear();


  // Compute first the eigenvalues for u (as in the previous function)
  // and then for \delta u

  // Compute eigenvalues/vectors
  double E_eigenvalue_1, E_eigenvalue_2;
  Tensor<2,dim> P_matrix;
  eigen_vectors_and_values(E_eigenvalue_1, E_eigenvalue_2,P_matrix,E);

  double E_eigenvalue_1_plus = std::max(0.0, E_eigenvalue_1);
  double E_eigenvalue_2_plus = std::max(0.0, E_eigenvalue_2);

  Tensor<2,dim> Lambda_plus;
  Lambda_plus[0][0] = E_eigenvalue_1_plus;
  Lambda_plus[0][1] = 0.0;
  Lambda_plus[1][0] = 0.0;
  Lambda_plus[1][1] = E_eigenvalue_2_plus;

  if (!derivative)
    {
      Tensor<2,dim> E_plus = P_matrix * Lambda_plus * transpose(P_matrix);

      double tr_E_positive = std::max(0.0, tr_E);

      stress_term_plus = lame_coefficient_lambda * tr_E_positive * Identity
                         + 2 * lame_coefficient_mu * E_plus;

      stress_term_minus = lame_coefficient_lambda * (tr_E - tr_E_positive) * Identity
                          + 2 * lame_coefficient_mu * (E - E_plus);
    }
  else
    {
      // Derviatives (\delta u)

      // Compute eigenvalues/vectors
      double E_eigenvalue_1_LinU, E_eigenvalue_2_LinU;
      Tensor<1,dim> E_eigenvector_1_LinU;
      Tensor<1,dim> E_eigenvector_2_LinU;
      Tensor<2,dim> P_matrix_LinU;

      // Compute linearized Eigenvalues
      double diskriminante = std::sqrt(E[0][1] * E[1][0] + (E[0][0] - E[1][1]) * (E[0][0] - E[1][1])/4.0);

      E_eigenvalue_1_LinU = 0.5 * tr_E_LinU + 1.0/(2.0 * diskriminante) *
                            (E_LinU[0][1] * E[1][0] + E[0][1] * E_LinU[1][0] + (E[0][0] - E[1][1])*(E_LinU[0][0] - E_LinU[1][1])/2.0);

      E_eigenvalue_2_LinU = 0.5 * tr_E_LinU - 1.0/(2.0 * diskriminante) *
                            (E_LinU[0][1] * E[1][0] + E[0][1] * E_LinU[1][0] + (E[0][0] - E[1][1])*(E_LinU[0][0] - E_LinU[1][1])/2.0);


      // Compute normalized Eigenvectors and P
      double normalization_1 = 1.0/(std::sqrt(1 + (E_eigenvalue_1 - E[0][0])/E[0][1] * (E_eigenvalue_1 - E[0][0])/E[0][1]));
      double normalization_2 = 1.0/(std::sqrt(1 + (E_eigenvalue_2 - E[0][0])/E[0][1] * (E_eigenvalue_2 - E[0][0])/E[0][1]));

      double normalization_1_LinU = 0.0;
      double normalization_2_LinU = 0.0;

      normalization_1_LinU = -1.0 * (1.0/(1.0 + (E_eigenvalue_1 - E[0][0])/E[0][1] * (E_eigenvalue_1 - E[0][0])/E[0][1])
                                     * 1.0/(2.0 * std::sqrt(1.0 + (E_eigenvalue_1 - E[0][0])/E[0][1] * (E_eigenvalue_1 - E[0][0])/E[0][1]))
                                     * (2.0 * (E_eigenvalue_1 - E[0][0])/E[0][1])
                                     * ((E_eigenvalue_1_LinU - E_LinU[0][0]) * E[0][1] - (E_eigenvalue_1 - E[0][0]) * E_LinU[0][1])/(E[0][1] * E[0][1]));

      normalization_2_LinU = -1.0 * (1.0/(1.0 + (E_eigenvalue_2 - E[0][0])/E[0][1] * (E_eigenvalue_2 - E[0][0])/E[0][1])
                                     * 1.0/(2.0 * std::sqrt(1.0 + (E_eigenvalue_2 - E[0][0])/E[0][1] * (E_eigenvalue_2 - E[0][0])/E[0][1]))
                                     * (2.0 * (E_eigenvalue_2 - E[0][0])/E[0][1])
                                     * ((E_eigenvalue_2_LinU - E_LinU[0][0]) * E[0][1] - (E_eigenvalue_2 - E[0][0]) * E_LinU[0][1])/(E[0][1] * E[0][1]));


      E_eigenvector_1_LinU[0] = normalization_1 * 1.0;
      E_eigenvector_1_LinU[1] = normalization_1 * (E_eigenvalue_1 - E[0][0])/E[0][1];

      E_eigenvector_2_LinU[0] = normalization_2 * 1.0;
      E_eigenvector_2_LinU[1] = normalization_2 * (E_eigenvalue_2 - E[0][0])/E[0][1];


      // Apply product rule to normalization and vector entries
      double EV_1_part_1_comp_1 = 0.0;  // LinU in vector entries, normalization U
      double EV_1_part_1_comp_2 = 0.0;  // LinU in vector entries, normalization U
      double EV_1_part_2_comp_1 = 0.0;  // vector entries U, normalization LinU
      double EV_1_part_2_comp_2 = 0.0;  // vector entries U, normalization LinU

      double EV_2_part_1_comp_1 = 0.0;  // LinU in vector entries, normalization U
      double EV_2_part_1_comp_2 = 0.0;  // LinU in vector entries, normalization U
      double EV_2_part_2_comp_1 = 0.0;  // vector entries U, normalization LinU
      double EV_2_part_2_comp_2 = 0.0;  // vector entries U, normalization LinU

      // Effizienter spaeter, aber erst einmal uebersichtlich und verstehen!
      EV_1_part_1_comp_1 = normalization_1 * 0.0;
      EV_1_part_1_comp_2 = normalization_1 *
                           ((E_eigenvalue_1_LinU - E_LinU[0][0]) * E[0][1] - (E_eigenvalue_1 - E[0][0]) * E_LinU[0][1])/(E[0][1] * E[0][1]);

      EV_1_part_2_comp_1 = normalization_1_LinU * 1.0;
      EV_1_part_2_comp_2 = normalization_1_LinU * (E_eigenvalue_1 - E[0][0])/E[0][1];


      EV_2_part_1_comp_1 = normalization_2 * 0.0;
      EV_2_part_1_comp_2 = normalization_2 *
                           ((E_eigenvalue_2_LinU - E_LinU[0][0]) * E[0][1] - (E_eigenvalue_2 - E[0][0]) * E_LinU[0][1])/(E[0][1] * E[0][1]);

      EV_2_part_2_comp_1 = normalization_2_LinU * 1.0;
      EV_2_part_2_comp_2 = normalization_2_LinU * (E_eigenvalue_2 - E[0][0])/E[0][1];



      // Build eigenvectors
      E_eigenvector_1_LinU[0] = EV_1_part_1_comp_1 + EV_1_part_2_comp_1;
      E_eigenvector_1_LinU[1] = EV_1_part_1_comp_2 + EV_1_part_2_comp_2;

      E_eigenvector_2_LinU[0] = EV_2_part_1_comp_1 + EV_2_part_2_comp_1;
      E_eigenvector_2_LinU[1] = EV_2_part_1_comp_2 + EV_2_part_2_comp_2;



      // P-Matrix
      P_matrix_LinU[0][0] = E_eigenvector_1_LinU[0];
      P_matrix_LinU[0][1] = E_eigenvector_2_LinU[0];
      P_matrix_LinU[1][0] = E_eigenvector_1_LinU[1];
      P_matrix_LinU[1][1] = E_eigenvector_2_LinU[1];


      double E_eigenvalue_1_plus_LinU = 0.0;
      double E_eigenvalue_2_plus_LinU = 0.0;


      // Very important: Set E_eigenvalue_1_plus_LinU to zero when
      // the corresponding rhs-value is set to zero and NOT when
      // the value itself is negative!!!
      if (E_eigenvalue_1 < 0.0)
        {
          E_eigenvalue_1_plus_LinU = 0.0;
        }
      else
        E_eigenvalue_1_plus_LinU = E_eigenvalue_1_LinU;


      if (E_eigenvalue_2 < 0.0)
        {
          E_eigenvalue_2_plus_LinU = 0.0;
        }
      else
        E_eigenvalue_2_plus_LinU = E_eigenvalue_2_LinU;



      Tensor<2,dim> Lambda_plus_LinU;
      Lambda_plus_LinU[0][0] = E_eigenvalue_1_plus_LinU;
      Lambda_plus_LinU[0][1] = 0.0;
      Lambda_plus_LinU[1][0] = 0.0;
      Lambda_plus_LinU[1][1] = E_eigenvalue_2_plus_LinU;

      Tensor<2,dim> E_plus_LinU = P_matrix_LinU * Lambda_plus * transpose(P_matrix) +  P_matrix * Lambda_plus_LinU * transpose(P_matrix) + P_matrix * Lambda_plus * transpose(P_matrix_LinU);


      double tr_E_positive_LinU = 0.0;
      if (tr_E < 0.0)
        {
          tr_E_positive_LinU = 0.0;

        }
      else
        tr_E_positive_LinU = tr_E_LinU;



      stress_term_plus = lame_coefficient_lambda * tr_E_positive_LinU * Identity
                         + 2 * lame_coefficient_mu * E_plus_LinU;

      stress_term_minus = lame_coefficient_lambda * (tr_E_LinU - tr_E_positive_LinU) * Identity
                          + 2 * lame_coefficient_mu * (E_LinU - E_plus_LinU);
    }
}


//-----------------------------------------------------------------------------
// Class to perform local cell/face-level computations
template <int dim>
class CGElasticityPhaseFieldEquations
{
public:
    CGElasticityPhaseFieldEquations(InputHandler &parser);

    void set_min_cell_diameter(const double set_min_cell_diameter);

    void set_dynamic_PF_parameters();

    void get_solution_and_timestep_data(
            BlockVector<double> gsolution,
            BlockVector<double> gold_timestep_solution,
            BlockVector<double> gold_old_timestep_solution,
            BlockVector<double> gsolution_lambda_penal_func,
            double gtime,
            unsigned int gtimestep_number,
            double gtimestep,
            double gold_timestep,
            double gold_old_timestep);

    void assemble_cell_term_matrix(const FEValues<dim>& fe_v,
            const double cell_dia,
            FullMatrix<double> &ui_phi_i_matrix) const;

    void assemble_cell_term_rhs(const FEValues<dim>& fe_v,
            const double cell_dia,
            Vector<double> &cell_vector) const;

    double lame_coefficient_mu_global();
    double lame_coefficient_lambda_global();

private:
    InputHandler &parser;
    TestCase testcase;
    LinearizationScheme lintype;
    OptimizationType opttype;
    BlockVector<double> solution, old_timestep_solution, old_old_timestep_solution;
    BlockVector<double> solution_lambda_penal_func;
    double theta_DG;
    double time, timestep, old_timestep, old_old_timestep;
    unsigned int timestep_number;
    double lame_coefficient_mu, lame_coefficient_lambda;
    double constant_k, alpha_eps, G_c, gamma_penal, penalty_u_bc, penalty_u_face;
    double min_cell_diameter;
    bool decompose_stress_matrix, decompose_stress_rhs, scale_penalty;
};

//-----------------------------------------------------------------------------
// Constructor: populate parameters from InputHandler class object reference
template <int dim>
CGElasticityPhaseFieldEquations<dim>::
CGElasticityPhaseFieldEquations (InputHandler &parser) :
    parser(parser)
{
    // Determine test case
    if (parser.get_testcase() == "miehe tension")
        testcase = TestCase::miehe_tension;
    else if (parser.get_testcase() == "miehe shear")
        testcase = TestCase::miehe_shear;
    else if (parser.get_testcase() == "symmetric bending")
        testcase = TestCase::symmetric_bending;
    else
        testcase = TestCase::none;

    // Set linearization type
    if (parser.get_lintype() == "simple")
        lintype = LinearizationScheme::simple;
    else if (parser.get_lintype() == "interpolation")
        lintype = LinearizationScheme::interpolation;
    else
        lintype = LinearizationScheme::none;

    // Set optimization type
    if (parser.get_opttype() == "simple penalization")
        opttype = OptimizationType::simple;
    else if (parser.get_opttype() == "augmented lagrange")
        opttype = OptimizationType::auglag;
    else
        opttype = OptimizationType::none;

    // Populate parameters
    penalty_u_bc = parser.get_penaltyBC();      // Penalty for Dirichlet BC
    penalty_u_face  = parser.get_penaltyDG();   // Penalty for internal faces
    gamma_penal = parser.get_penaltyAL();       // Aug-Lagrangian penalization
    theta_DG = parser.get_sform();              // DG method parameter

    // Phase-field regularization parameters
    // alpha_eps could be formulated in terms
    // of the minimal `h' (see min_cell_diameter)
    // alpha_eps gives the width of the transition zone
    constant_k = parser.get_pfkappa();
    alpha_eps = parser.get_pfepsilon();  // 4.4e-2 // alpha_eps = 2h

    // Critical energy release rate
    G_c = parser.get_criticalGc();

    // Solid parameters
    lame_coefficient_mu = parser.get_lamemu();
    lame_coefficient_lambda =  parser.get_lamelambda();

    decompose_stress_matrix = parser.get_decomposestressmatrix();
    decompose_stress_rhs = parser.get_decomposestressrhs ();
    scale_penalty = parser.get_scalepenalty();
}

//-----------------------------------------------------------------------------
// Inject data from class DGMethod<dim> back into cell-level routines
template <int dim>
void CGElasticityPhaseFieldEquations<dim>::get_solution_and_timestep_data (
        BlockVector<double> gsolution,
        BlockVector<double> gold_timestep_solution,
        BlockVector<double> gold_old_timestep_solution,
        BlockVector<double> gsolution_lambda_penal_func,
        double gtime,
        unsigned int gtimestep_number,
        double gtimestep,
        double gold_timestep,
        double gold_old_timestep)
{
    solution = gsolution;
    old_timestep_solution = gold_timestep_solution;
    old_old_timestep_solution = gold_old_timestep_solution;
    solution_lambda_penal_func = gsolution_lambda_penal_func;
    time = gtime;
    timestep_number = gtimestep_number;
    timestep = gtimestep;
    old_timestep = gold_timestep;
    old_old_timestep = gold_old_timestep;
}

template <int dim>
void CGElasticityPhaseFieldEquations<dim>::set_min_cell_diameter(const double set_min_cell_diameter)
{
    min_cell_diameter = set_min_cell_diameter;
}

template <int dim>
void CGElasticityPhaseFieldEquations<dim>::set_dynamic_PF_parameters()
{
    alpha_eps = 0.125 * std::pow(min_cell_diameter, 0.25);
    constant_k = 1e-10 * min_cell_diameter;
}

template <int dim>
double CGElasticityPhaseFieldEquations<dim>::lame_coefficient_mu_global ()
{
    return lame_coefficient_mu;
}

template <int dim>
double CGElasticityPhaseFieldEquations<dim>::lame_coefficient_lambda_global ()
{
    return lame_coefficient_lambda;
}

template <int dim>
void CGElasticityPhaseFieldEquations<dim>::assemble_cell_term_matrix(const FEValues<dim> &fe_values,
        const double cell_dia,
        FullMatrix<double> &ui_phi_i_matrix) const
{
    const unsigned int   dofs_per_cell   = fe_values.dofs_per_cell;
    const unsigned int   n_q_points = fe_values.n_quadrature_points;
    const std::vector<double> &JxW = fe_values.get_JxW_values ();
    const FEValuesExtractors::Vector displacements (0);
    const FEValuesExtractors::Scalar phase_field (2);

    std::vector<Vector<double> > old_solution_values (n_q_points,
            Vector<double>(dim+1));

    std::vector<std::vector<Tensor<1,dim> > > old_solution_grads (n_q_points,
            std::vector<Tensor<1,dim> > (dim+1));

    std::vector<Vector<double> > old_timestep_solution_values (n_q_points,
            Vector<double>(dim+1));

    std::vector<std::vector<Tensor<1,dim> > > old_timestep_solution_grads (n_q_points,
            std::vector<Tensor<1,dim> > (dim+1));

    std::vector<Vector<double> > old_old_timestep_solution_values (n_q_points,
            Vector<double>(dim+1));

    std::vector<Vector<double> > old_solution_lambda_penal_func_values (n_q_points,
            Vector<double>(dim+1));

    std::vector<Tensor<1,dim> > phi_i_u (dofs_per_cell);
    std::vector<Tensor<2,dim> > phi_i_grads_u(dofs_per_cell);
    std::vector<double>         phi_i_pf(dofs_per_cell);
    std::vector<Tensor<1,dim> > phi_i_grads_pf (dofs_per_cell);
    std::vector<Tensor<2,dim>>  E_LinU (dofs_per_cell);
    std::vector<Tensor<2,dim>>  sigma_phi_LinU_plus(dofs_per_cell);
    std::vector<Tensor<2,dim>>  sigma_phi_LinU_minus(dofs_per_cell);

    fe_values.get_function_values (solution, old_solution_values);
    fe_values.get_function_grads (solution, old_solution_grads);

    fe_values.get_function_values (old_timestep_solution, old_timestep_solution_values);
    fe_values.get_function_grads (old_timestep_solution, old_timestep_solution_grads);

    fe_values.get_function_values (old_old_timestep_solution, old_old_timestep_solution_values);
    fe_values.get_function_values (solution_lambda_penal_func, old_solution_lambda_penal_func_values);

    Tensor<2,dim> identity = Tensors::get_Identity<dim> ();

    for (unsigned int q=0; q<n_q_points; ++q)
    {
        // Compute pf_extra
        double pf_extra = 0.0;
        const double pf = Tensors::get_pf<dim> (q, old_solution_values);
        const double old_timestep_pf = Tensors::get_pf<dim> (q, old_timestep_solution_values);
        const double old_old_timestep_pf = Tensors::get_pf<dim> (q, old_old_timestep_solution_values);
        {
            if (lintype == LinearizationScheme::simple)
                pf_extra = old_timestep_pf;
            else if (lintype == LinearizationScheme::interpolation)
                pf_extra = interpolate_pf(old_timestep_pf, old_old_timestep_pf,
                        old_timestep, old_old_timestep);
            else
                abort();

            pf_extra = normalize_pf(pf_extra);
        }

        // Compute outer loop optimization
        double chi = 0.0;
        switch(opttype)
        {
            case OptimizationType::simple :
                if ((pf - old_timestep_pf) > 0)
                    chi = 1.0 / (cell_dia * cell_dia);
                else
                    chi = 0.0;
                break;

            case OptimizationType::auglag :
                {
                    const double lambda_penal_func = Tensors::get_lambda_penal<dim>(q, old_solution_lambda_penal_func_values);
                    if ((lambda_penal_func + gamma_penal * (pf - old_timestep_pf)) > 0)
                        chi = 1.0;
                    else
                        chi = 0.0;
                    break;
                }

            default :
                abort();
                break;
        }

        // Obtain previous newton iteration data
        Tensor<2,dim> grad_u = Tensors::get_grad_u<dim> (q, old_solution_grads);
        const Tensor<2,dim> E_u = 0.5 * (grad_u + transpose(grad_u));
        Tensor<2,dim> sigma_ui_plus, sigma_ui_minus;
        if (decompose_stress_matrix && timestep_number>1)
        {
            Tensor<2,dim> zero_matrix;
            zero_matrix.clear();

            decompose_stress(sigma_ui_plus, sigma_ui_minus,
                    E_u, trace(E_u), zero_matrix, 0.0,
                    lame_coefficient_lambda, lame_coefficient_mu, false);

        }
        else
        {
            sigma_ui_plus   = 2.0 * lame_coefficient_mu * E_u
                + lame_coefficient_lambda * trace(E_u) * identity;
            sigma_ui_minus  = 0.0;
        }

        // Compute and store desired quantities
        for (unsigned int k=0; k<dofs_per_cell; ++k)
        {
            phi_i_pf[k]             = fe_values[phase_field].value (k, q);
            phi_i_grads_pf[k]       = fe_values[phase_field].gradient (k, q);
            phi_i_u[k]          = fe_values[displacements].value (k, q);
            phi_i_grads_u[k]    = fe_values[displacements].gradient (k, q);
            E_LinU[k] = 0.5 * (phi_i_grads_u[k] + transpose(phi_i_grads_u[k]));
            if ((decompose_stress_matrix) && (timestep_number>1))
            {
                decompose_stress(sigma_phi_LinU_plus[k], sigma_phi_LinU_minus[k],
                        E_u, trace(E_u), E_LinU[k], trace(E_LinU[k]),
                        lame_coefficient_lambda, lame_coefficient_mu, true);
            }
            else
            {
                sigma_phi_LinU_plus[k]  = 2.0 * lame_coefficient_mu * E_LinU[k]
                    + lame_coefficient_lambda * trace(E_LinU[k]) * identity;
                sigma_phi_LinU_minus[k] = 0.0;                
            }
        }


        for (unsigned int i=0; i<dofs_per_cell; ++i)
        {
            for (unsigned int j=0; j<dofs_per_cell; ++j)
            {              
              // Outer loop optimization
              ui_phi_i_matrix(j,i) += JxW[q] * chi * gamma_penal * phi_i_pf[i] * phi_i_pf[j];

              // Energy regularization
              ui_phi_i_matrix(j,i) += timestep * JxW[q] *
                  (((1-constant_k) * phi_i_pf[i] * scalar_product(sigma_ui_plus, E_u) * phi_i_pf[j]) +
                   G_c / alpha_eps * phi_i_pf[i] * phi_i_pf[j] +
                   G_c * alpha_eps * phi_i_grads_pf[i] * phi_i_grads_pf[j]);
                
              // Cross terms
              ui_phi_i_matrix(j,i) += timestep * JxW[q] *
                  ((1-constant_k) * 
                    (scalar_product(sigma_phi_LinU_plus[i], E_u) +
                     scalar_product(sigma_ui_plus, E_LinU[i])) * pf * phi_i_pf[j]);
          
              // Displacements
              ui_phi_i_matrix(j,i) += timestep * JxW[q] *
                (scalar_product(((1 - constant_k) * pf_extra * pf_extra + constant_k) *
                  sigma_phi_LinU_plus[i], E_LinU[j]) +
                scalar_product(sigma_phi_LinU_minus[i], E_LinU[j]));
            }
        }
    } // end q_point
}

template <int dim>
void CGElasticityPhaseFieldEquations<dim>::assemble_cell_term_rhs(const FEValues<dim> &fe_values,
        const double cell_dia,
        Vector<double> &local_rhs) const
{
    const unsigned int   dofs_per_cell   = fe_values.dofs_per_cell;
    const unsigned int   n_q_points = fe_values.n_quadrature_points;
    const std::vector<double> &JxW = fe_values.get_JxW_values ();
    const FEValuesExtractors::Vector displacements (0);
    const FEValuesExtractors::Scalar phase_field (2);

    std::vector<Vector<double> > old_solution_values (n_q_points,
            Vector<double>(dim+1));

    std::vector<std::vector<Tensor<1,dim> > > old_solution_grads (n_q_points,
            std::vector<Tensor<1,dim> > (dim+1));

    std::vector<Vector<double> > old_timestep_solution_values (n_q_points,
            Vector<double>(dim+1));

    std::vector<std::vector<Tensor<1,dim> > > old_timestep_solution_grads (n_q_points,
            std::vector<Tensor<1,dim> > (dim+1));

    std::vector<Vector<double> > old_old_timestep_solution_values (n_q_points,
            Vector<double>(dim+1));

    std::vector<Vector<double> > old_solution_lambda_penal_func_values (n_q_points,
            Vector<double>(dim+1));

    fe_values.get_function_values (solution, old_solution_values);
    fe_values.get_function_grads (solution, old_solution_grads);

    fe_values.get_function_values (old_timestep_solution, old_timestep_solution_values);
    fe_values.get_function_grads (old_timestep_solution, old_timestep_solution_grads);

    fe_values.get_function_values (old_old_timestep_solution, old_old_timestep_solution_values);
    fe_values.get_function_values (solution_lambda_penal_func, old_solution_lambda_penal_func_values);

    Tensor<2,dim> identity = Tensors::get_Identity<dim> ();

    for (unsigned int q=0; q<n_q_points; ++q)
    {
        // Compute pf_extra
        double pf_extra = 0.0;
        const double pf = Tensors::get_pf<dim> (q, old_solution_values);
        const double old_timestep_pf = Tensors::get_pf<dim> (q, old_timestep_solution_values);
        const double old_old_timestep_pf = Tensors::get_pf<dim> (q, old_old_timestep_solution_values);

        {
            if (lintype == LinearizationScheme::simple)
                pf_extra = old_timestep_pf;
            else if (lintype == LinearizationScheme::interpolation)
                pf_extra = interpolate_pf(old_timestep_pf, old_old_timestep_pf,
                        old_timestep, old_old_timestep);
            else
                abort();
            pf_extra = normalize_pf(pf_extra);
        }

        // Outer loop optimization
        double pf_minus_old_timestep_pf_plus = 0.0;
        switch (opttype)
        {
            case OptimizationType::simple :
                pf_minus_old_timestep_pf_plus = gamma_penal / (cell_dia * cell_dia) * (pf - old_timestep_pf);
                break;

            case OptimizationType::auglag :
                {
                    const double lambda_penal_func = Tensors::get_lambda_penal<dim>(q, old_solution_lambda_penal_func_values);
                    pf_minus_old_timestep_pf_plus = lambda_penal_func + gamma_penal * (pf - old_timestep_pf);
                    break;
                }

            default:
                abort();
                break;
        }
        pf_minus_old_timestep_pf_plus = std::max(0.0, pf_minus_old_timestep_pf_plus);

        // Get old newton step data
        Tensor<1,dim> grad_pf = Tensors::get_grad_pf<dim>(q, old_solution_grads);
        Tensor<2,dim> grad_u = Tensors::get_grad_u<dim>(q, old_solution_grads);
        Tensor<2,dim> E_u = 0.5 * (grad_u + transpose(grad_u));
        Tensor<2,dim> sigma_ui_plus, sigma_ui_minus;
        if (decompose_stress_rhs && timestep_number>1)
        {
            Tensor<2,dim> zero_matrix;
            zero_matrix.clear();

            decompose_stress(sigma_ui_plus, sigma_ui_minus,
                    E_u, trace(E_u), zero_matrix, 0.0,
                    lame_coefficient_lambda, lame_coefficient_mu, false);
        }
        else
        {
            sigma_ui_plus = 2.0 * lame_coefficient_mu * E_u + lame_coefficient_lambda * trace(E_u) * identity;
            sigma_ui_minus = 0;
        }

        for (unsigned int i=0; i<dofs_per_cell; ++i)
        {
            const unsigned int comp_i = fe_values.get_fe().system_to_component_index(i).first;
            if (comp_i == dim)
            {
                const double phi_i_pf = fe_values[phase_field].value(i,q);
                const Tensor<1,dim> phi_i_grads_pf = fe_values[phase_field].gradient(i,q);

                // Augmented Lagrangian OR Simple Penalization
                local_rhs(i) -= JxW[q] * pf_minus_old_timestep_pf_plus * phi_i_pf;

                // Phase-field and cross terms
                local_rhs(i) -= timestep * JxW[q] *
                    ((1.0 - constant_k) * pf * scalar_product(sigma_ui_plus, E_u) * phi_i_pf 
                    - G_c/alpha_eps * (1.0 - pf) * phi_i_pf
                    + G_c * alpha_eps * grad_pf * phi_i_grads_pf);
            }
            else
            {
                const Tensor<2,dim> phi_i_grads_u = fe_values[displacements].gradient(i,q);
                const Tensor<2,dim> E_phi = 0.5 * (phi_i_grads_u + transpose(phi_i_grads_u));

                // Displacement
                local_rhs(i) -= timestep * JxW[q] *
                    (scalar_product(((1 - constant_k) * pf_extra * pf_extra + constant_k) * sigma_ui_plus, E_phi) +
                     scalar_product(sigma_ui_minus, E_phi));
            }
        } // end i
    } // end q_point
}

//----------------------------------------------------------
// Class for initial values
template <int dim>
class InitialValues : public Function<dim>
{
    public:
        InitialValues () : Function<dim>(dim+1) {}

        virtual double value (const Point<dim>   &p,
                const unsigned int  component = 0) const;

        virtual void vector_value (const Point<dim> &p,
                Vector<double>   &value) const;
};

template <int dim>
double InitialValues<dim>::value (const Point<dim>  &p,
        const unsigned int component) const
{
    if (component == 2)
        return 1.0;
    else
        return 0.0;
}

template <int dim>
void InitialValues<dim>::vector_value (const Point<dim> &p,
        Vector<double>   &values) const
{
    for (unsigned int comp=0; comp<this->n_components; ++comp)
        values (comp) = InitialValues<dim>::value (p, comp);
}

//----------------------------------------------------------------
// Class for miehe-tension boundary values
template<int dim>
class BoundaryTension : public Function<dim>
{
public:
  BoundaryTension (const double time) : Function<dim> (dim+1)
  {
    _time = time;
  }

  virtual double value (const Point<dim> &p, const unsigned int component = 0) const;

  virtual void vector_value (const Point<dim> &p, Vector<double> &value) const;

private:
  double _time;
};

template<int dim>
double BoundaryTension<dim>::value(const Point<dim> &p, const unsigned int component) const
{
  Assert (component < this->n_components,
    ExcIndexRange(component, 0, this->n_components));

  double disp_per_timestep = 1.0;

  if (component == 1)
  {
    return (  ((p(1) == 1.0) && (p(0) <= 1.0) && (p(0) >= 0.0))
      ?
      (1.0) * _time * disp_per_timestep : 0.0 );
  }

  return 0.0;
}

template<int dim>
void BoundaryTension<dim>::vector_value (const Point<dim> &p, Vector<double> &value) const
{
  for (unsigned int c=0; c<this->n_components; ++c)
    value(c) = BoundaryTension<dim>::value(p, c);
}

//----------------------------------------------------------------
// Class for miehe-shear boundary values
template<int dim>
class BoundaryShear : public Function<dim>
{
public:
  BoundaryShear (const double time) : Function<dim> (dim+1)
  {
    _time = time;
  }

  virtual double value (const Point<dim> &p, const unsigned int component = 0) const;

  virtual void vector_value (const Point<dim> &p, Vector<double> &value) const;

private:
  double _time;
};

template<int dim>
double BoundaryShear<dim>::value(const Point<dim> &p, const unsigned int component) const
{
  Assert (component < this->n_components,
    ExcIndexRange(component, 0, this->n_components));

  double disp_per_timestep = 1.0;

  if (component == 0)
  {
    return (  ((p(1) == 1.0))     // && (p(0) <= 1.0) && (p(0) >= 0.0))
      ?
      (-1.0) * _time * disp_per_timestep : 0.0 );
  }

  return 0.0;
}

template<int dim>
void BoundaryShear<dim>::vector_value (const Point<dim> &p, Vector<double> &value) const
{
  for (unsigned int c=0; c<this->n_components; ++c)
    value(c) = BoundaryShear<dim>::value(p, c);
}

//-----------------------------------------------------------------------------
// Flow of control class
template <int dim>
class DGMethod
{
    public:
        DGMethod (InputHandler &parser);
        ~DGMethod ();

        void run ();

    private:
        void set_runtime_parameters ();
        void setup_system ();
        void setup_constraints ();
        void apply_dirichlet_bc ();
        void assemble_system_matrix ();
        void assemble_system_rhs ();
        double newton_iteration();

        void solve ();
        double get_next_timestep (bool& cut_timestep, double tmp_timestep) const;
        void refine_standard ();
        bool predictor_corrector ();
        void normalize_phase_field ();
        void output_results (const unsigned int cycle,
                const BlockVector<double> output_vector) const;

        void compute_surface_load();

        class Postprocessor;

        Triangulation<dim>   triangulation;
        const MappingQ1<dim> mapping;

        const unsigned int   degree;

        FESystem<dim>          fe;
        DoFHandler<dim>      dof_handler;

        ConstraintMatrix     constraints;

        BlockSparsityPattern      sparsity_pattern;
        BlockSparseMatrix<double> system_matrix;

        const QGauss<dim>   quadrature;
        const QGauss<dim-1> face_quadrature;

        BlockVector<double>       solution, old_timestep_solution, old_old_timestep_solution;
        BlockVector<double>       system_rhs, newton_update;
        BlockVector<double>       solution_lambda_penal_func;

        InputHandler &parser;
        TestCase testcase;
        OptimizationType opttype;
        CGElasticityPhaseFieldEquations<dim> cg;

        double gamma_penal;
        bool do_quick_test, do_adaptive_ref;

        // Global variables for timestepping scheme
        unsigned int timestep_number;
        unsigned int max_no_timesteps;
        unsigned int num_newton_iters;
        double timestep, time, tolerance_newton, 
            tolerance_auglag, old_timestep, old_old_timestep;
        std::string time_stepping_scheme;
        
        double min_cell_diameter;
        unsigned int max_penal_iterations;
};

template <int dim>
DGMethod<dim>::
DGMethod(InputHandler &parser):mapping (),
    degree(1),
    fe (FE_Q<dim>(degree), dim,
            FE_Q<dim>(degree), 1),
    dof_handler (triangulation),
    quadrature (degree+2),
    face_quadrature (degree+2),
    parser(parser),
    testcase(TestCase::miehe_tension),
    opttype(OptimizationType::simple),
    cg (parser)
{}

template <int dim>
DGMethod<dim>::~DGMethod ()
{
    dof_handler.clear ();
}

template <int dim>
void DGMethod<dim>::set_runtime_parameters ()
{
    // Set test case
    if (parser.get_testcase() == "miehe tension")
        testcase = TestCase::miehe_tension;
    else if (parser.get_testcase() == "miehe shear")
        testcase = TestCase::miehe_shear;
    else if (parser.get_testcase() == "symmetric bending")
        testcase = TestCase::symmetric_bending;
    else
        testcase = TestCase::none;

    // Set optimization type
    if (parser.get_opttype() == "simple penalization")
        opttype = OptimizationType::simple;
    else if (parser.get_opttype() == "augmented lagrange")
        opttype = OptimizationType::auglag;
    else
        opttype = OptimizationType::none;

    // Timestepping scheme (here backward
    // Euler since problem is quasi-static)
    time_stepping_scheme = "BE";

    // Timestep size:
    timestep = parser.get_timestep();

    // Maximum number of timesteps:
    max_no_timesteps = parser.get_maxtimesteps();

    // A variable to count the number of time steps
    timestep_number = 0;

    // Newton Tolerance
    tolerance_newton = parser.get_tolerancenewton();

    // Augmented Lagrange Tolerance
    tolerance_auglag = parser.get_toleranceAL();

    // Counts total time
    time = 0;

    // Gamma Penal
    gamma_penal = parser.get_penaltyAL();;

    // Max number of Augmented Lagrange loops
    max_penal_iterations = parser.get_maxitersAL();

    if (testcase == TestCase::miehe_tension || testcase == TestCase::miehe_shear)
    {
        //std::cout << "Setting up";
        //abort();
        std::string grid_name;
        grid_name  = "unit_slit.inp";
        GridIn<dim> grid_in;
        grid_in.attach_triangulation (triangulation);
        std::ifstream input_file(grid_name.c_str());
        Assert (dim==2, ExcInternalError());
        grid_in.read_ucd (input_file);
        triangulation.refine_global (parser.get_globalrefines());
    }
    else
    {
        std::cout << "40112: Not implemented" << std::endl;
        abort();
    }

    do_adaptive_ref = parser.get_predictorcorrector();
    do_quick_test = parser.get_quicktest();
}

template <int dim>
void DGMethod<dim>::setup_system ()
{
    system_matrix.clear ();
    dof_handler.distribute_dofs (fe);
    DoFRenumbering::Cuthill_McKee (dof_handler);
    std::vector<unsigned int> block_component (3,0);
    block_component[2] = 1; // phase-field
    DoFRenumbering::component_wise (dof_handler, block_component);
    {
        constraints.clear ();
        setup_constraints();
        DoFTools::make_hanging_node_constraints (dof_handler,
                constraints);
    }
    constraints.close ();

    std::vector<unsigned int> dofs_per_block (2);
    DoFTools::count_dofs_per_block (dof_handler, dofs_per_block, block_component);
    const unsigned int n_u = dofs_per_block[0],
    n_c = dofs_per_block[1];

    std::cout << "Cells:\t"
        << triangulation.n_active_cells()
        << std::endl
        << "DoFs:\t"
        << dof_handler.n_dofs()
        << " (" << n_u << '+' << n_c << ')'
        << std::endl;

    BlockCompressedSimpleSparsityPattern csp (2,2);

    csp.block(0,0).reinit (n_u, n_u);
    csp.block(0,1).reinit (n_u, n_c);

    csp.block(1,0).reinit (n_c, n_u);
    csp.block(1,1).reinit (n_c, n_c);

    csp.collect_sizes();

    DoFTools::make_sparsity_pattern (dof_handler, csp, constraints, false);

    sparsity_pattern.copy_from (csp);
    system_matrix.reinit (sparsity_pattern);

    // Actual solution at time step n
    solution.reinit (2);
    solution.block(0).reinit (n_u);
    solution.block(1).reinit (n_c);

    solution.collect_sizes ();

    // Old timestep solution at time step n-1
    old_timestep_solution.reinit (2);
    old_timestep_solution.block(0).reinit (n_u);
    old_timestep_solution.block(1).reinit (n_c);

    old_timestep_solution.collect_sizes ();

    //!mital
    // Old old timestep solution at time step n-2
    old_old_timestep_solution.reinit (2);
    old_old_timestep_solution.block(0).reinit (n_u);
    old_old_timestep_solution.block(1).reinit (n_c);

    old_old_timestep_solution.collect_sizes ();

    // Updates for Newton's method
    newton_update.reinit (2);
    newton_update.block(0).reinit (n_u);
    newton_update.block(1).reinit (n_c);

    newton_update.collect_sizes ();

    // Residual for  Newton's method
    system_rhs.reinit (2);
    system_rhs.block(0).reinit (n_u);
    system_rhs.block(1).reinit (n_c);

    system_rhs.collect_sizes ();

    solution_lambda_penal_func.reinit(2);
    solution_lambda_penal_func.block(0).reinit(n_u);
    solution_lambda_penal_func.block(1).reinit(n_c);

    solution_lambda_penal_func.collect_sizes();

        min_cell_diameter = 1.0e+10;
    double cell_diameter = 1.0e+9;
    typename DoFHandler<dim>::active_cell_iterator
        cell = dof_handler.begin_active(),
             endc = dof_handler.end();

    for (; cell!=endc; ++cell)
    {
        cell_diameter = cell->diameter();
        if (cell_diameter < min_cell_diameter)
            min_cell_diameter = cell_diameter;
    }

    std::cout << "Min cell dia: " << min_cell_diameter << std::endl;
    cg.set_min_cell_diameter(min_cell_diameter);

    if (parser.get_dynamicKE())
        cg.set_dynamic_PF_parameters();
}

template<int dim>
void DGMethod<dim>::setup_constraints ()
{
    std::vector<bool> component_mask (dim+1, false);

    if (testcase == TestCase::miehe_tension)
    {
        component_mask[0] = true;
        component_mask[1] = false;
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            0, ZeroFunction<dim>(dim+1), constraints, component_mask);

        component_mask[0] = true;
        component_mask[1] = false;
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            1, ZeroFunction<dim>(dim+1), constraints, component_mask);

        component_mask[0] = false;
        component_mask[1] = true;
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            2, ZeroFunction<dim>(dim+1), constraints, component_mask);

        component_mask[0] = false;
        component_mask[1] = true;
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            3, ZeroFunction<dim>(dim+1), constraints, component_mask);                                    
    }

    if (testcase == TestCase::miehe_shear)
    {
        component_mask[0] = false;
        component_mask[1] = true;   // on left: restrict u_y
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            0, ZeroFunction<dim>(dim+1), constraints, component_mask);

        component_mask[0] = false;
        component_mask[1] = true;   // on right: restrict u_y
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            1, ZeroFunction<dim>(dim+1), constraints, component_mask);

        component_mask[0] = true;   // on bottom: restrict u_x and u_y 
        component_mask[1] = true;   // i.e. clamped
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            2, ZeroFunction<dim>(dim+1), constraints, component_mask);

        component_mask[0] = true;   // on top: restrict u_x
        component_mask[1] = false;
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            3, ZeroFunction<dim>(dim+1), constraints, component_mask);

        component_mask[0] = false;   // on crack: restrict u_y
        component_mask[1] = true;
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            4, ZeroFunction<dim>(dim+1), constraints, component_mask);
    }
}

template<int dim>
void DGMethod<dim>::apply_dirichlet_bc ()
{
    std::map<unsigned int, double> boundary_values;
    std::vector<bool> component_mask(dim+1, false);
    
    if (testcase == TestCase::miehe_tension)
    {
        component_mask[0] = true;
        component_mask[1] = false;
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            0, ZeroFunction<dim>(dim+1), boundary_values, component_mask);

        component_mask[0] = true;
        component_mask[1] = false;
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            1, ZeroFunction<dim>(dim+1), boundary_values, component_mask);

        component_mask[0] = false;
        component_mask[1] = true;
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            2, ZeroFunction<dim>(dim+1), boundary_values, component_mask);

        component_mask[0] = false;
        component_mask[1] = true;
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            3, BoundaryTension<dim>(time), boundary_values, component_mask);                                    
    }
    else if (testcase == TestCase::miehe_shear)
    {
        component_mask[0] = false;
        component_mask[1] = true;   // left: u_y specified 
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            0, ZeroFunction<dim>(dim+1), boundary_values, component_mask);

        component_mask[0] = false;
        component_mask[1] = true;   // right: u_y specified
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            1, ZeroFunction<dim>(dim+1), boundary_values, component_mask);

        component_mask[0] = true;   // bottom: u_x, u_y specified
        component_mask[1] = true;
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            2, ZeroFunction<dim>(dim+1), boundary_values, component_mask);

        component_mask[0] = true;   // top: u_x specified
        component_mask[1] = false;
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            3, BoundaryShear<dim>(time), boundary_values, component_mask);

        component_mask[0] = false;   // crack surface (if I had to guess)
        component_mask[1] = true;
        component_mask[2] = false;
        VectorTools::interpolate_boundary_values (dof_handler, 
            4, ZeroFunction<dim>(dim+1), boundary_values, component_mask);                                    
    }

    for (typename std::map<unsigned int, double>::const_iterator i = boundary_values.begin();
        i!= boundary_values.end(); ++i)
        solution(i->first) = i->second;
}

// Jacobian of Newton's method
template <int dim>
void DGMethod<dim>::assemble_system_matrix ()
{
    system_matrix=0;

    const unsigned int dofs_per_cell = dof_handler.get_fe().dofs_per_cell;
    std::vector<unsigned int> dofs (dofs_per_cell);
    std::vector<unsigned int> dofs_neighbor (dofs_per_cell);

    const UpdateFlags update_flags = update_values
        | update_gradients
        | update_quadrature_points
        | update_JxW_values;

    const UpdateFlags face_update_flags = update_values
        | update_gradients
        | update_quadrature_points
        | update_JxW_values
        | update_normal_vectors;

    FEValues<dim> fe_values (mapping, fe, quadrature, update_flags);
    FEFaceValues<dim> fe_values_face (mapping, fe, face_quadrature, face_update_flags);

    FullMatrix<double> ui_phi_i_matrix (dofs_per_cell, dofs_per_cell);

    cg.get_solution_and_timestep_data (
            solution,
            old_timestep_solution,
            old_old_timestep_solution,
            solution_lambda_penal_func,
            time,
            timestep_number,
            timestep,
            old_timestep,
            old_old_timestep);

    typename DoFHandler<dim>::active_cell_iterator
        cell = dof_handler.begin_active(),
             endc = dof_handler.end();

    for (;cell!=endc; ++cell)
    {
        ui_phi_i_matrix = 0;
        fe_values.reinit (cell);

        cg.assemble_cell_term_matrix(fe_values,
                cell->diameter(),
                ui_phi_i_matrix);

        cell->get_dof_indices (dofs);

        constraints.distribute_local_to_global (ui_phi_i_matrix, dofs, system_matrix);
    }
}


// Right hand side residual of Newton's method
template <int dim>
void DGMethod<dim>::assemble_system_rhs ()
{
    system_rhs=0;

    const unsigned int dofs_per_cell = dof_handler.get_fe().dofs_per_cell;
    std::vector<unsigned int> dofs (dofs_per_cell);
    std::vector<unsigned int> dofs_neighbor (dofs_per_cell);

    const UpdateFlags update_flags = update_values
        | update_gradients
        | update_quadrature_points
        | update_JxW_values;

    const UpdateFlags face_update_flags = update_values
        | update_gradients
        | update_quadrature_points
        | update_JxW_values
        | update_normal_vectors;

    FEValues<dim> fe_values (mapping, fe, quadrature, update_flags);
    FEFaceValues<dim> fe_values_face (mapping, fe, face_quadrature, face_update_flags);
    
    Vector<double> ui_phi_i_rhs (dofs_per_cell);

    cg.get_solution_and_timestep_data (
            solution,
            old_timestep_solution,
            old_old_timestep_solution,
            solution_lambda_penal_func,
            time,
            timestep_number,
            timestep,
            old_timestep,
            old_old_timestep);

    typename DoFHandler<dim>::active_cell_iterator
        cell = dof_handler.begin_active(),
        endc = dof_handler.end();

    for (;cell!=endc; ++cell)
    {
        ui_phi_i_rhs = 0;
        fe_values.reinit (cell);

        cg.assemble_cell_term_rhs(fe_values,
                cell->diameter(),
                ui_phi_i_rhs);

        cell->get_dof_indices (dofs);
        constraints.distribute_local_to_global (ui_phi_i_rhs, dofs, system_rhs);

        //  Loop over boundary faces and apply non-homogenous Neumann BCs if any here
    }
}


// Newton's method with simple line search backtracking
template <int dim>
double DGMethod<dim>::newton_iteration ()
{
    Timer timer_newton;
    const double lower_bound_newton_residuum = tolerance_newton;
    const unsigned int max_no_newton_steps  = parser.get_maxitersnewt();

    // Decision whether the system matrix should be build
    // at each Newton step
    const double nonlinear_rho = 0.1;

    // Line search parameters
    unsigned int line_search_step;
    const unsigned int  max_no_line_search_steps = 10;
    const double line_search_damping = 0.6;
    double new_newton_residuum;

    //assemble_system_matrix();
    apply_dirichlet_bc();
    assemble_system_rhs();
    //solve();

    double newton_residuum = system_rhs.linfty_norm();
    double old_newton_residuum= newton_residuum;
    unsigned int newton_step = 1;

    if (newton_residuum < lower_bound_newton_residuum)
    {
        std::cout << '\t'
            << std::scientific
            << newton_residuum
            << std::endl;
    }

    while (newton_residuum > lower_bound_newton_residuum &&
            newton_step < max_no_newton_steps)
    {
        timer_newton.start();
        old_newton_residuum = newton_residuum;

        assemble_system_rhs();
        newton_residuum = system_rhs.linfty_norm();

        if (newton_residuum < lower_bound_newton_residuum)
        {
            std::cout << '\t'
                << std::scientific << newton_residuum << std::endl;
            break;
        }

        if (newton_residuum/old_newton_residuum > nonlinear_rho)
            assemble_system_matrix ();

        // Solve Ax = b
        solve ();

        line_search_step = 0;
        for ( ;line_search_step < max_no_line_search_steps; ++line_search_step)
        {
            solution += newton_update;
            assemble_system_rhs ();
            new_newton_residuum = system_rhs.linfty_norm();

            if (new_newton_residuum < newton_residuum)
                break;
            else
                solution -= newton_update;

            if (line_search_step == max_no_line_search_steps-1)
                solution +=newton_update;

            newton_update *= line_search_damping;
        }

        timer_newton.stop();

        std::cout << std::setprecision(5) <<newton_step << '\t'
            << std::scientific << newton_residuum << '\t'
            << std::scientific << newton_residuum/old_newton_residuum  <<'\t' ;

        if (newton_residuum/old_newton_residuum > nonlinear_rho)
            std::cout << "r" << '\t' ;
        else
            std::cout << " " << '\t' ;

        std::cout << line_search_step  << '\t'
            << std::scientific << timer_newton ()
            << std::endl;

        // Updates
        timer_newton.reset();
        newton_step++;
    }

    num_newton_iters += newton_step;
    return newton_residuum;
}


template <int dim>
void DGMethod<dim>::solve ()
{
    Vector<double> sol, rhs;
    sol = newton_update;
    rhs = system_rhs;

    SparseDirectUMFPACK A_direct;
    A_direct.factorize(system_matrix);
    A_direct.vmult(sol,rhs);
    newton_update = sol;

    constraints.distribute (newton_update);
}


template<int dim>
void DGMethod<dim>::normalize_phase_field ()
{
    for (unsigned int k = 0;
            k<solution.block(1).size();
            ++k)
        solution.block(1)(k) =
            normalize_pf(solution.block(1)(k));
}

//-----------------------------------------------------------------------------
// Data output for Enriched Galerkin simplified
template<int dim> 
class DGMethod<dim>::Postprocessor : public DataPostprocessor<dim>
{
public:
    Postprocessor(): DataPostprocessor<dim>() {}
    
    virtual ~Postprocessor() {}
    
    virtual void compute_derived_quantities_vector (
        const std::vector<Vector<double>> &uh,
        const std::vector<std::vector<Tensor<1,dim>>> &duh,
        const std::vector<std::vector<Tensor<2,dim>>> &dduh,
        const std::vector<Point<dim>> &normals,
        const std::vector<Point<dim>> &evaluation_points,
        std::vector<Vector<double>> &computed_quantities) const;
    
    virtual std::vector<std::string> get_names () const;

    virtual
    std::vector<DataComponentInterpretation::DataComponentInterpretation>
    get_data_component_interpretation () const;

    virtual UpdateFlags get_needed_update_flags () const;   
};

template<int dim>
std::vector<std::string>
DGMethod<dim>::Postprocessor::get_names() const {
    std::vector<std::string> solution_names(1, "Ux");
    solution_names.push_back("Uy");
    solution_names.push_back("phi");
    return solution_names;
}

template <int dim>
std::vector<DataComponentInterpretation::DataComponentInterpretation>
DGMethod<dim>::Postprocessor::get_data_component_interpretation () const {
  std::vector<DataComponentInterpretation::DataComponentInterpretation>
  interpretation (dim,
      DataComponentInterpretation::component_is_part_of_vector);
  interpretation.push_back (DataComponentInterpretation::component_is_scalar);
  return interpretation;
}

template <int dim>
UpdateFlags
DGMethod<dim>::Postprocessor::get_needed_update_flags() const
{
  return update_values | update_quadrature_points;
}

template <int dim>
void DGMethod<dim>::Postprocessor::compute_derived_quantities_vector (
    const std::vector<Vector<double>> &uh,
    const std::vector<std::vector<Tensor<1,dim>>> &/*duh*/,
    const std::vector<std::vector<Tensor<2,dim>>> &/*dduh*/,
    const std::vector<Point<dim>> &/*normals*/,
    const std::vector<Point<dim>> &evaluation_points,
    std::vector<Vector<double>> &computed_quantities) const
{
  const unsigned int n_quadrature_points = uh.size();
  Assert (computed_quantities.size() == n_quadrature_points, ExcInternalError());
  Assert (evaluation_points.size() == n_quadrature_points, ExcInternalError());
  Assert (uh[0].size() == 3, ExcInternalError());
  for (unsigned int q=0; q<n_quadrature_points; ++q)
  {
    computed_quantities[q][0] = uh[q][0];
    computed_quantities[q][1] = uh[q][1];
    computed_quantities[q][2] = uh[q][2];
  }
}


template <int dim>
void DGMethod<dim>::output_results (const unsigned int cycle,
        const BlockVector<double> output_vector) const
{
    Postprocessor postprocessor;
    DataOut<dim> data_out;
    data_out.attach_dof_handler (dof_handler);
    data_out.add_data_vector (solution, postprocessor);
    data_out.build_patches ();

    //TODO: change filename basis
    std::string filename_basis;
    filename_basis  = "solution_crack_ex_1_dg_4_";

    std::ostringstream filename;
    std::cout << "------------------" << std::endl;
    std::cout << "Write solution" << std::endl;
    std::cout << "------------------" << std::endl;
    std::cout << std::endl;
    filename << filename_basis
        << Utilities::int_to_string (cycle, 5)
        << ".vtu";

    std::ofstream output (filename.str().c_str());
    data_out.write_vtu (output);
}


// Now, we arrive at the function that is responsible
// to compute the line integrals for the surface load
// evaluation.
template <int dim>
void DGMethod<dim>::compute_surface_load()
{
    const QGauss<dim-1> face_quadrature_formula (3);
    FEFaceValues<dim> fe_face_values (fe, face_quadrature_formula,
            update_values | update_gradients | update_normal_vectors |
            update_JxW_values);

    const unsigned int dofs_per_cell = fe.dofs_per_cell;
    const unsigned int n_face_q_points = face_quadrature_formula.size();

    std::vector<unsigned int> local_dof_indices (dofs_per_cell);
    std::vector<Vector<double> >  old_solution_values (n_face_q_points,
            Vector<double> (dim+1));

    std::vector<std::vector<Tensor<1,dim> > >
        old_solution_grads (n_face_q_points, std::vector<Tensor<1,dim> > (dim+1));

    Tensor<1,dim> drag_lift_value, u_face_int;
    double area_face_int = 0.0;
    drag_lift_value.clear();
    u_face_int.clear();

    Tensor<2,dim> identity = Tensors::get_Identity<dim> ();

    double lame_coefficient_mu = cg.lame_coefficient_mu_global();
    double lame_coefficient_lambda = cg.lame_coefficient_lambda_global();

    typename DoFHandler<dim>::active_cell_iterator
        cell = dof_handler.begin_active(),
             endc = dof_handler.end();

    for (; cell!=endc; ++cell)
    {
        for (unsigned int face=0; face<GeometryInfo<dim>::faces_per_cell; ++face)
            if (cell->face(face)->at_boundary() &&
                    cell->face(face)->boundary_indicator()==3)
            {
                fe_face_values.reinit (cell, face);
                fe_face_values.get_function_values (solution, old_solution_values);
                fe_face_values.get_function_grads (solution, old_solution_grads);

                for (unsigned int q=0; q<n_face_q_points; ++q)
                {
                    const double dx = fe_face_values.JxW(q);

                    Tensor<2,dim> grad_u = Tensors::get_grad_u<dim>(q, old_solution_grads);
                    Tensor<1,dim> u_bc = Tensors::get_u<dim>(q, old_solution_values);

                    Tensor<2,dim> E = 0.5 * (grad_u + transpose(grad_u));
                    Tensor<2,dim> sigma_s = 2.0 * lame_coefficient_mu * E +
                        lame_coefficient_lambda * trace(E) * identity;

                    drag_lift_value +=  sigma_s *
                        fe_face_values.normal_vector(q) * dx;

                    u_face_int += u_bc * dx;

                    area_face_int += dx;
                }
            } // end boundary 3 for structure
    }

    Tensor<1,dim> u_bc = u_face_int/area_face_int;
    std::cout << "Fx: " << u_bc[0] << "  " << drag_lift_value[0] << std::endl;
    std::cout << "Fy: " << u_bc[1] << "  " << drag_lift_value[1] << std::endl;
}


template<int dim>
void DGMethod<dim>::refine_standard()
{   
    typename DoFHandler<dim>::active_cell_iterator
        cell = dof_handler.begin_active(),
             endc = dof_handler.end();

    for (; cell!=endc; ++cell)
    {
        for (unsigned int vertex=0;vertex < GeometryInfo<dim>::vertices_per_cell;++vertex)
        {
            if (testcase == TestCase::miehe_tension)
            {
                Tensor<1,dim> cell_vertex = (cell->vertex(vertex));
                if (cell_vertex[0] <= 0.55 && cell_vertex[0] >= 0.0 &&
                        cell_vertex[1] < 0.55 && cell_vertex[1] > 0.45)
                {
                    cell->set_refine_flag();
                    break;
                }
            }
            else if (testcase == TestCase::miehe_shear)
            {
                Tensor<1,dim> cell_vertex = (cell->vertex(vertex));
                if (cell_vertex[0] <= 0.55 && cell_vertex[0] >= 0.0 &&
                        cell_vertex[1] <= 0.55 && cell_vertex[1] >= 0.0)
                {
                    cell->set_refine_flag();
                    break;
                }
            }
        }
    }

    BlockVector<double> tmp_solution;
    tmp_solution = solution;

    SolutionTransfer<dim, BlockVector<double> > solution_transfer (dof_handler);

    triangulation.prepare_coarsening_and_refinement();
    solution_transfer.prepare_for_coarsening_and_refinement(tmp_solution);

    triangulation.execute_coarsening_and_refinement ();
    setup_system ();

    solution_transfer.interpolate(tmp_solution, solution);
}

template<int dim>
bool DGMethod<dim>::predictor_corrector()
{
    bool redo_step = false;
    const unsigned int max_ref_lvl = parser.get_globalrefines() + 2;
    const double pf_threshold = parser.get_refpfthreshold();        
    const unsigned int n_q_points = quadrature.size();

    const UpdateFlags update_flags = update_values;
    FEValues<dim> fe_values (mapping, fe, quadrature, update_flags);
    std::vector<Vector<double> >  old_solution_values (n_q_points,
        Vector<double> (dim+1));

    typename DoFHandler<dim>::active_cell_iterator
        cell = dof_handler.begin_active(),
             endc = dof_handler.end();

    for (; cell!=endc; ++cell)
    {
        fe_values.reinit(cell);
        fe_values.get_function_values(solution, old_solution_values);

        for (unsigned int q=0; q<n_q_points; ++q)
        {
            const double pf = Tensors::get_pf<dim>(q, old_solution_values);
            if ((pf < pf_threshold) && (cell->level() < max_ref_lvl))
            {
                cell->set_refine_flag();
                redo_step = true;
                break;
            }
        }
    }

    // Discard the converged step and project (n-1) and (n-2) time-level solutions
    //    onto the new mesh. Keep solution_lambda_penal_func from converged solution
    //    as this might (investigate) result in quicker convergence of the repeated step.
    // Note: we interpolate lambda irrespective of the type of outer loop optimization
    //    being used since this is relatively inexpensive.

    if (redo_step)
    {
        std::vector<BlockVector<double>> tmp_sol(4);
        tmp_sol[0] = old_timestep_solution;
        tmp_sol[1] = old_old_timestep_solution;
        tmp_sol[2] = solution_lambda_penal_func;
        tmp_sol[3] = solution;

        SolutionTransfer<dim, BlockVector<double>> solution_transfer(dof_handler);
        triangulation.prepare_coarsening_and_refinement();
        solution_transfer.prepare_for_coarsening_and_refinement(tmp_sol);
        triangulation.execute_coarsening_and_refinement ();
        
        setup_system ();

        std::vector<BlockVector<double>> fin_sol(4);
        fin_sol[0] = old_timestep_solution;
        fin_sol[1] = old_old_timestep_solution;
        fin_sol[2] = solution_lambda_penal_func;
        fin_sol[3] = solution;
        solution_transfer.interpolate(tmp_sol, fin_sol);

        old_timestep_solution = fin_sol[0];
        old_old_timestep_solution = fin_sol[1];
        solution_lambda_penal_func = fin_sol[2];        
        solution = fin_sol[3];
    }
    return redo_step;
}

template<int dim>
inline double DGMethod<dim>::get_next_timestep(bool& cut_timestep, double tmp_timestep) const 
{
    if (cut_timestep)
    {
        cut_timestep = false;
        return tmp_timestep;
    }
    else if (do_quick_test)
    {
        if (time >= 5.0e-3 && testcase == TestCase::miehe_tension)
            return 5.0e-6;
        else if (time >= 8.5e-3 && testcase == TestCase::miehe_shear)
            return 5.0e-6;
        else
            return timestep;
    }
    else
        return timestep;
}


template <int dim>
void DGMethod<dim>::run ()
{
    set_runtime_parameters ();
    setup_system ();

    // A-Priori mesh refinement
    if (parser.get_refapriori())
      for (unsigned int iter=0; iter<2; ++iter)
        refine_standard();

    std::cout 
        << "\n====================================="
        << "\nParameters"
        << "\n====================================="
        << std::endl;

    // Output a summary of the parameters
    //echo_parameters();

    {
        ConstraintMatrix constraints;
        constraints.close();

        //Initialize the solution vector
        VectorTools::project (dof_handler,
                constraints,
                QGauss<dim>(degree+2),
                InitialValues<dim>(),
                solution);

        output_results (0,solution);
    }

    // Initialize Vectors
    old_timestep_solution = solution;
    solution_lambda_penal_func = 0;

    // Initialize old and old_old timestep sizes
    old_timestep = timestep;
    old_old_timestep = timestep;

    const unsigned int output_skip = parser.get_outputskip();

    bool cut_timestep = false;
    double tmp_timestep = 0.0;

    // Time loop
    do
    {
        // Shift timestepping parameters
        old_old_timestep = old_timestep;
        old_timestep = timestep;
        timestep = get_next_timestep(cut_timestep, tmp_timestep);

        // shift solution vectors
        old_old_timestep_solution = old_timestep_solution;
        old_timestep_solution = solution;
        
        // Jump back for predictor-corrector and cutting timestep
        recompute_last_step:

        std::cout << "Timestep [Num:Time:Size]" 
            << " : [ " << std::scientific << timestep_number
            << " : " << time
            << " : " << timestep << " ]"
            << "\n=============================="
            << "====================================="
            << std::endl << std::endl;   


        double Newton_error = 0.0;
        double L2_error = 0.0;
        num_newton_iters = 0;

        if (opttype == OptimizationType::auglag)
        {
            BlockVector<double> solution_lambda_penal_func_difference,
                old_solution_lambda_penal_func;
            unsigned int penal_iterations = 0;

            do
            {
                // Step 1: Solve the system
                Newton_error = newton_iteration ();

                // Step 2: Update lambda_penal_func
                old_solution_lambda_penal_func = solution_lambda_penal_func;
                for (unsigned int j=0; j<solution_lambda_penal_func.size(); ++j)
                {
                    double lambda_penal_update = std::max(0.0, (solution_lambda_penal_func(j)
                        + gamma_penal * (solution(j) - old_timestep_solution(j))));

                    solution_lambda_penal_func(j) = lambda_penal_update;
                }

                solution_lambda_penal_func_difference = solution_lambda_penal_func;
                solution_lambda_penal_func_difference -= old_solution_lambda_penal_func;

                Vector<float> difference_per_cell (triangulation.n_active_cells());
                const ComponentSelectFunction<dim> pf_solution_mask (dim, dim+1);
                VectorTools::integrate_difference (dof_handler,
                        solution_lambda_penal_func_difference,
                        ZeroFunction<dim>(dim+1),
                        difference_per_cell,
                        QGauss<dim>(degree+2),
                        VectorTools::L2_norm,
                        &pf_solution_mask);
                L2_error = difference_per_cell.l2_norm();

                std::cout << std::endl;
                std::cout << "Norm Lambda Difference: " << std::scientific
                    << L2_error << std::endl;
                std::cout << std::endl;

                penal_iterations++;
            }
            while ((L2_error > tolerance_auglag) && (penal_iterations < max_penal_iterations));

            std::cout << std::endl << "Number of Augmented Lagrange Loops: " << std::scientific
                << penal_iterations << std::endl;
            std::cout << std::endl << "Number of Newton Iterations: " << std::scientific 
                << num_newton_iters << std::endl;
        }
        else if (opttype == OptimizationType::simple)
        {
            Newton_error = newton_iteration();
        }
        else
        {
            abort();
        }

        // Catch non-converged cases here and cut the timestep if desired
        if((L2_error > tolerance_auglag || Newton_error > tolerance_newton) && parser.get_cuttimestep())
        {
            if (!cut_timestep)
            {
                cut_timestep = true;
                tmp_timestep = timestep;
            }

            solution = old_timestep_solution;
            timestep = timestep/2;
            old_timestep = timestep;

            if (timestep <= 1e-9)
            {
                std::cout << "Timestep too small. Aborting...";
                abort();
            }
            else
            {  
                std::cout << "------------------" << std::endl;
                std::cout << "Cutting Timestep" << std::endl;
                std::cout << "------------------" << std::endl;
            }

            goto recompute_last_step;
        }
        else if (L2_error > tolerance_auglag || Newton_error > tolerance_newton)
        {
            std::cout << std::endl << "Could not converge.";
            if ((L2_error > (50 * tolerance_auglag)) || (Newton_error > (50 * tolerance_newton)))
            {
                std::cout << std::endl << "Residuals are too large. Aborting...";
                abort();
            }
            else
                std::cout << std::endl << "Residuals are small enough. Continuing..." << std::endl;
        }


        // Perform Predictor-Corrector refinement
        if (do_adaptive_ref) 
        {
            bool changed = predictor_corrector();
            if (changed)
            {
                std::cout << "------------------" << std::endl;
                std::cout << "Mesh Changed" << std::endl;
                std::cout << "------------------" << std::endl;
                std::cout << std::endl;
                solution = old_timestep_solution;
                goto recompute_last_step;
            }
        }

        time += timestep;

        // Compute functional values
        if (testcase == TestCase::miehe_tension ||
            testcase == TestCase::miehe_shear)
            compute_surface_load();

        // Write solutions
        if ((timestep_number % output_skip == 0))
            output_results (timestep_number+1,solution);
        
        // Uncertain if this is needed
        //std::cout << std::endl;
        //normalize_phase_field();

        ++timestep_number;

    }
    while (time <= parser.get_maxtime());
}


int main (int argc, char **argv)
{

    InputHandler parser(argc, argv);

    try
    {
        using namespace dealii;
        const unsigned int dim = 2;
        {
            DGMethod<dim> dgmethod_iso(parser);
            dgmethod_iso.run ();
        }
    }
    catch (std::exception &exc)
    {
        std::cerr << std::endl << std::endl
            << "----------------------------------------------------"
            << std::endl;

        std::cerr << "Exception on processing: " << std::endl
            << exc.what() << std::endl
            << "Aborting!" << std::endl
            << "----------------------------------------------------"
            << std::endl;

        return 1;
    }
    catch (...)
    {
        std::cerr << std::endl << std::endl
            << "----------------------------------------------------"
            << std::endl;

        std::cerr << "Unknown exception!" << std::endl
            << "Aborting!" << std::endl
            << "----------------------------------------------------"
            << std::endl;

        return 1;
    };
    return 0;
}

// TODO
// - penalization parameter depends on time-dependent phase-field
// - DONE simple penalization -> might be improved by augmented Lagrangian
// - u as DG and phi as CG (maybe also phi in DG)
// - SIPG, NIPG, IIPG require careful testing and comparions
// - DONE make penalization parameter `h' or 'face-length' dependent
// - a hybrid DG/CG formulation in which DG is only solved `around' the crack
// - shear test: decomposition into tension and compression
