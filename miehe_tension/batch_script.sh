DG=$1
for PENALTY in NoGamma WGamma
do
    DIR="study_${DG}_${PENALTY}"
    echo ${DIR}
    cd $DIR
    echo "Now in `pwd`"
    make clean
    make sent
    cd ..
    pwd
done
