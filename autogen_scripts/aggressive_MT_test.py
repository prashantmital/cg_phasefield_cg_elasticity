"""
Script to generate the parameter files and
run the deal.II jobs
"""

import shutil
import os
from itertools import product
from numpy import ceil
from datetime import datetime
from sys import argv

def get_template(fname):
    '''
    Read the template data from file called fname
    '''
    with open(fname, 'r') as fh:
        data = fh.readlines()
    return data

def merge_dicts(*dict_args):
    '''
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    '''
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result

def get_prm_format():
    prm_key = {
            "method":"No corresponding entry in PRM file",
            "boundary_p":"set Boundary Term Penalty",
            "scale_penalty":"set Scale Penalty",
            "face_p":"set Face Term Penalty",
            "sform":"set sform",
            "Gc":"set Critical Fracture Energy",
            "mu":"set Lame's Parameter Mu",
            "lambda":"set Lame's Parameter Lambda",
            "epsilon":"set Constant E",
            "kappa":"set Constant K",
            "linscheme":"set Linearization Scheme",
            "decomp_mat":"set Decompose Stress Matrix",
            "decomp_rhs":"set Decompose Stress RHS",
            "refines":"set Num Global Refines",
            "output_skip":"set Output Skip",
            "quicktest":"set Quick Test",
            "endtime":"set Simulation End Time",
            "testcase":"set Test Case",
            "timestep":"set Timestep Size",
            "ALtolerance":"set AugLag Tolerance",
            "maxALiters":"set Maximum Penalty Iterations",
            "NWtolerance":"set Newton Tolerance",
            "opttype":"set Optimization Type",
            "gamma":"set Penalty Gamma"
            }
    return prm_key


def parameter_space():
    '''
    Create a dictionary with the correct prm entries
    '''
    dg_methods = ["OBB", "NIPG", "IIPG"]
    
    # Fixed
    boun_penalties = ["0", "1e+10", "1e+10"]
    face_penalties = ["0", "1e+14", "1e+14"]
    sforms = ["1", "1", "0"]
    scale_penalty = ['false']*3
    Gc = ["2.7"]*3
    lamb = ["121.15e+3"]*3
    mu = ["80.77e+3"]*3
    epsilon = ["4.4e-2"]*3
    kappa = ["1.0e-12"]*3
    linscheme = ["simple"]*3
    decomp_mat = ["true"]*3
    decomp_rhs = ["true"]*3
    #decomp_mat = ["false"]*3
    #decomp_rhs = ["false"]*3
    refines = ["3"]*3
    endtime = ["6.5e-3"]*3
    testcase = ["miehe tension"]*3
    timesteps = ["1e-4"]*3
    quicktest = ['true']*3
    output_skips = [str(int(ceil((6.5e-3)/(float(y)*200)))) for y in timesteps]
    ALtolerance = ["1.0e-4"]*3
    maxALiters = ["500"]*3
    NWtolerance = ["5.0e-8"]*3
    opttype = ["augmented lagrange"]*3
    #opttype = ["simple penalization"]*3
    gamma = ["1e-15"]*3
    methods = list()
    for i in range(len(dg_methods)):
        _m = {  
                "method":dg_methods[i],
                "boundary_p":boun_penalties[i],
                "face_p":face_penalties[i],
                "scale_penalty":scale_penalty[i],
                "sform":sforms[i],
                "Gc":Gc[i],
                "lambda":lamb[i],
                "mu":mu[i],
                "epsilon":epsilon[i],
                "kappa":kappa[i],
                "linscheme":linscheme[i],
                "decomp_mat":decomp_mat[i],
                "decomp_rhs":decomp_rhs[i],
                "refines":refines[i],
                "output_skip":output_skips[i],
                "quicktest":quicktest[i],
                "endtime":endtime[i],
                "testcase":testcase[i],
                "timestep":timesteps[i],
                "ALtolerance":ALtolerance[i],
                "maxALiters":maxALiters[i],
                "NWtolerance":NWtolerance[i],
                "opttype":opttype[i],
                "gamma":gamma[i]
            }
        methods.append(_m)
    
    # Variable
    refines = [2, 3, 4]
    #opttype = ["simple penalization"]*2 + ["augmented lagrange"]*2
    timesteps = ["1e-4", "1e-5"]*2
    quicktest = ['true', 'false']*2
    scale_penalty = ['true', 'false']
    parameters = list()

    # Modify range argument to ensure all cases are created!
    for i in range(3):
        _p = {
                "refines":str(refines[i])
                #"timestep":timesteps[i],
                #"quicktest":quicktest[i],
                #"opttype":opttype[i],
                #"scale_penalty":scale_penalty[i]
                }
        parameters.append(_p)

    # Return all combinations
    return product(methods, parameters)


def generate_prm(job, template):
    '''
    Generate prm file contents while changing some parameters
    '''
    prm_format = get_prm_format()
    template[0] = "# " + str(job) + " \n"
    for index in range(len(template)):
        for key in job:
            if template[index].find(prm_format[key]) != -1:
                template[index] = (prm_format[key]
                        + " = " + job[key] + "\n")
            else:
                continue
    
    return template


if __name__=='__main__':
    # Generate job timestamp
    timestamp = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

    # Initialize log summary
    log = list()

    # Generate directory name
    if len(argv) == 1:
        job_name = "study_MT_" + timestamp
    else:
        _, descriptor = argv
        job_name = "study_MT_" + descriptor

    # Get initial data-template
    template = get_template("input_template.prm")

    # Create job directory
    os.mkdir(job_name)
    os.chdir(job_name)

    # Generate internal folder hirearchy and populate with files
    for (i, _case) in enumerate(parameter_space()):
        case = merge_dicts(*_case)
        case_data = generate_prm(case, template)
        case_name = case["method"] + "_id_" + str(i)

        # Create case subdirectory and copy auxfiles into it
        shutil.copytree("../auxfiles", case_name, symlinks=False)

        # Dive into case directory
        os.chdir(case_name)

        # Write the parameter file
        with open("input_miehe.prm", 'w') as f1:
            f1.writelines(case_data)

        # Create a symlink to the executable
        os.symlink("../../step-30", "step-30")

        # Update the log
        log.append(case_name + "-->" + str(case) + "\n")

        # Come back to job directory
        os.chdir("..")
    # end for

    # Write log file
    with open("summary_" + timestamp +".log", 'w') as f1:
        f1.writelines(log)

    shutil.copy("../sweep_execute.sh", "./sweep_execute.sh")

    # Switch back to parent directory
    os.chdir("..")
    print "DONE"
